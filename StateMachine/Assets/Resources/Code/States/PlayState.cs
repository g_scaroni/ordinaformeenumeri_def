﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;
using Assets.Resources.Code.Scripts;

namespace Assets.Resources.Code.States
{
    class PlayState : IStateBase
    {
        private StateManager manager;   // manager game class type
        private GameObject shooter;     // reference to shooter object
        public Shooter shooterScript;  // reference to the script of the shooter

        /// <summary>
        /// Create the state
        /// </summary>
        /// <returns></returns>
        public PlayState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class
         
            // Load Scene01
            if (Application.loadedLevelName != "Scene01")
            {
                Application.LoadLevel("Scene01");
            }

            shooter = GameObject.Find("Shooter");   // find Shooter object in the Scene01

            shooterScript = shooter.GetComponent<Shooter>();    // assign shooter script
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void StateUpdate()
        {
            // if mouse is present update the force value
            if (Input.mousePresent)
            {
                // pass to the function the bool value to simulate decrease and increase of force
                manager.gameDataRef.forceManager.UpdateForce(Input.GetKey(KeyCode.A), Input.GetKey(KeyCode.B));

                // if timer is active update it
                if (manager.gameDataRef.forceManager.IsTimerActive )
                {
                    shooterScript.UpdateShooter(manager.gameDataRef.forceManager.updateTimer());
                }
                    // else if I'm in threshold and at costant value start it
                else if (manager.gameDataRef.forceManager.InThereshold && manager.gameDataRef.forceManager.InStdDevForveLimit ) manager.gameDataRef.forceManager.StartTimer();
            }
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void ShowIt()
        {
            // draw the texture store in instructionStateSplash variable that is the size of the screen
            GUI.DrawTexture(new Rect(Screen.width / 2 - 256 / 2, Screen.height / 2 - 128 / 2, 256, 128), manager.gameDataRef.playStateSplash, ScaleMode.StretchToFill);
            
            // show timer of force bar and std deviation of force
            GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Play state/ timer:= " + (int)manager.gameDataRef.forceManager.remainingTime + " " + (int)manager.gameDataRef.forceManager.StdDevForce);

            //draw the background:
            GUI.BeginGroup(new Rect(10, 10,(Screen.width - 20) * (1000 * 0.1f / 100f), 20));
            GUI.Box(new Rect(0, 0, (Screen.width - 20) * (manager.gameDataRef.forceManager.ForceLowerLimit * 0.1f / 100f), 20), manager.gameDataRef.backgroundBar);
            GUI.Box(new Rect(0, 0, (Screen.width - 20) * (manager.gameDataRef.forceManager.ForceUpperLimit * 0.1f / 100f), 20), manager.gameDataRef.redBar);
            GUI.Box(new Rect(0, 0, (Screen.width - 20) * (manager.gameDataRef.forceManager.ForceLowerLimit * 0.1f / 100f), 20), manager.gameDataRef.yellowBar);

            //draw the filled-in part:
            GUI.BeginGroup(new Rect(0, 0, (Screen.width - 20) * (manager.gameDataRef.forceManager.actualForce * 0.1f / 100f), 20));
            GUI.Box(new Rect(0, 0, (Screen.width - 20) * (manager.gameDataRef.forceManager.actualForce * 0.1f / 100f), 20), manager.gameDataRef.greenBar);
            GUI.EndGroup();
            GUI.EndGroup();

            GUI.backgroundColor = Color.white;
            // if button pressed or W keys
            if (GUI.Button(new Rect(10, 25, 250, 60), "Press Here or Any W to Won") || Input.GetKeyUp(KeyCode.W))
            {
                // switch the state to Setup state
                manager.SwitchState(new WonState(manager));
            }

            // if button pressed or L keys
            if (GUI.Button(new Rect(Screen.width/2 - 125, 25, 250, 60), "Press Here or Any L to Lose") || Input.GetKeyUp(KeyCode.L))
            {
                // switch the state to Setup state
                manager.SwitchState(new LostState(manager));
            }

            // if button pressed or R keys
            if (GUI.Button(new Rect(Screen.width - 250 - 5, 25, 250, 60), "Press Here or Any R to Return") || Input.GetKeyUp(KeyCode.R))
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }

            Debug.Log("In PlayState");
        }
    }
}